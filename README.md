# iocStats conda recipe

Home: https://github.com/epics-modules/iocStats

Package license: EPICS Open License (https://epics.anl.gov/license/open.php)

Recipe license: BSD 3-Clause

Summary: EPICS IOC Status and Control

